<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    public function index()
    {
        $student = Student::get();

        return response()->json([
            "status" => 1,
            "message" => "success",
            "data" => $student
        ], 200);
    }

    public function create(Request $request)
    {
        $request->validate([
            "name" => 'required',
            "no_tlp" => 'required',
            "email" => 'required',
            "alamat" => 'required',
        ]);

        $student = new Student();
        $student->name = $request->name;
        $student->no_tlp = $request->no_tlp;
        $student->email = $request->email;
        $student->alamat = $request->alamat;
        $student->save();

        return response()->json([
            "status" => 1,
            "message" => "success",
        ], 200);
    }

    public function edit($id)
    {
        $student = Student::find($id);

        return response()->json([
            "status" => 1,
            "message" => "success",
            "data" => $student
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $student = Student::find($id);

        $request->validate([
            "name" => 'required',
            "no_tlp" => 'required',
            "email" => 'required',
            "alamat" => 'required',
        ]);

        $student->update([
            "name" => $request->name,
            "no_tlp" => $request->no_tlp,
            "email" => $request->email,
            "alamat" => $request->alamat,
        ]);
        
        return response()->json([
            "status" => 1,
            "message" => "success",
        ], 200);
    }

    public function delete(Request $request, $id)
    {
        $student = Student::find($id)->delete();

        return response()->json([
            "status" => 1,
            "message" => "success",
        ], 200);
    }
}
